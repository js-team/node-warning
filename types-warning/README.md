# Installation
> `npm install --save @types/warning`

# Summary
This package contains type definitions for warning (https://github.com/BerkeleyTrue/warning).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/warning.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/warning/index.d.ts)
````ts
declare const warning: (condition: any, format?: string, ...extra: any[]) => void;
export = warning;

````

### Additional Details
 * Last updated: Wed, 18 Oct 2023 11:45:07 GMT
 * Dependencies: none

# Credits
These definitions were written by [Chi Vinh Le](https://github.com/cvle).
